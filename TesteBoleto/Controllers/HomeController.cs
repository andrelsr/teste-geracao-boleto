﻿using Boleto2Net;
using System;
using System.IO;
using System.Web.Mvc;

namespace TesteBoleto.Controllers {
    public class HomeController: Controller {
        public ActionResult Index() {
            return View();
        }

        public ActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact() {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private BoletoBancario constroiBoleto() {
            IBanco banco = Banco.Instancia(237);
            var objBoletos = new Boletos();

            objBoletos.Banco = banco;
            objBoletos.Banco.Cedente = new Cedente() {
                CPFCNPJ = "70.602.245/0001-74",
                Nome = "Empresa Cobradora",
                Observacoes = "Existe um débito de R$127,32. Caso já tenha sido pago favor desconsiderar."
            };

            var conta = new ContaBancaria() {
                Agencia = "1234",
                DigitoAgencia = "0",
                OperacaoConta = "1",
                Conta = "1234561",
                DigitoConta = "7",
                CarteiraPadrao = "09",


                //VariacaoCarteiraPadrao = "09",
                TipoCarteiraPadrao = TipoCarteira.CarteiraCobrancaSimples,
                TipoFormaCadastramento = TipoFormaCadastramento.ComRegistro,
                TipoImpressaoBoleto = TipoImpressaoBoleto.Empresa,
                TipoDocumento = TipoDocumento.Tradicional,

            };

            var endereco = new Endereco() {
                LogradouroEndereco = "Logradouro teste",
                LogradouroNumero = "1200",
                LogradouroComplemento = "Complemento teste",
                Bairro = "Meireles",
                Cidade = "Fortaleza",
                UF = "CE",
                CEP = "60-000-000"
            };

            objBoletos.Banco.Cedente.ContaBancaria = conta;
            objBoletos.Banco.Cedente.Endereco = endereco;

            var boleto = new Boleto(objBoletos.Banco);
            boleto.Sacado = new Sacado() {
                CPFCNPJ = "771.767.160-30",
                Endereco = new Endereco() {
                    LogradouroEndereco = "Av Dom Luis",
                    LogradouroNumero = "1200",
                    LogradouroComplemento = "Complemento",
                    Bairro = "Meireles",
                    CEP = "60-000-000",
                    Cidade = "Fortaleza",
                    UF = "CE"
                },
                Nome = "Nome pagador",
                Observacoes = "Pagar com urgencia"
            };
            boleto.CodigoOcorrencia = "01";
            boleto.DescricaoOcorrencia = "Remessa Registrar";
            boleto.NumeroDocumento = "01";
            boleto.NumeroControleParticipante = "12";
            boleto.NossoNumero = "4537";
            boleto.DataEmissao = DateTime.Now;
            boleto.DataVencimento = DateTime.Now.AddDays(15);
            boleto.ValorTitulo = 250.00m;
            boleto.Aceite = "N";
            boleto.EspecieDocumento = TipoEspecieDocumento.DM;
            boleto.DataDesconto = DateTime.Now.AddDays(15);
            boleto.ValorDesconto = 20m;

            boleto.DataMulta = DateTime.Now.AddDays(15);
            boleto.PercentualMulta = 2;
            boleto.ValorMulta = boleto.ValorTitulo * boleto.PercentualMulta / 100;
            boleto.MensagemInstrucoesCaixa = $"Cobrar multa de {boleto.ValorMulta} após a data de vencimento.";

            boleto.CodigoProtesto = TipoCodigoProtesto.NaoProtestar;
            boleto.DiasProtesto = 0;
            boleto.CodigoBaixaDevolucao = TipoCodigoBaixaDevolucao.NaoBaixarNaoDevolver;
            boleto.DiasBaixaDevolucao = 0;

            boleto.ValidarDados();

            objBoletos.Add(boleto);

            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string fileName = "arquivo_remessa.txt";
            if (System.IO.File.Exists($"{docPath}{fileName}"))
                System.IO.File.Delete($"{docPath}{fileName}");
            var remessa = new ArquivoRemessa(objBoletos.Banco, TipoArquivo.CNAB240, 1);
            var arquivo = new FileStream($"{docPath}{fileName}", FileMode.Create, FileAccess.ReadWrite);
            remessa.GerarArquivoRemessa(objBoletos, arquivo);

            //mst.WriteTo(arquivo);
            arquivo.Close();
            //mst.Close();
            //using (var mst = new MemoryStream()) {}
            BoletoBancario b = new BoletoBancario();
            b.Boleto = boleto;
            return b;
        }

        public ActionResult geraBoleto() {
            BoletoBancario b = constroiBoleto();
            ViewBag.Boleto = b.MontaHtmlEmbedded();
            return View();
        }

        public ActionResult geraBoletoPDF() {
            BoletoBancario b = constroiBoleto();
            var pdf = b.MontaBytesPDF();
            var file = File(pdf, "application/pdf");
            file.FileDownloadName = "boleto.pdf";
            return file;
        }
    }
}
